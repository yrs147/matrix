import numpy as np  
import os 

def rand_matrix(n):
    randmatrix = np.random.rand(n,n)
    return randmatrix

def invert(matrix):
    try:
        invmatrix = np.linalg.inv(matrix)
        return invmatrix
    except np.linalg.LinAlgError:
        print("Matrix cannnot be inverted")
        return None
    
def main():

    n = int(os.getenv("MATRIX_SIZE", 5000))

    randmatrix = rand_matrix(n)

    invertmatrix = invert(randmatrix)

    if invertmatrix is not None:
        print("\nInvert Matrix:")
        print(invertmatrix)

if __name__=="__main__":
    main()
