FROM python:3.9-slim

WORKDIR /usr/src/app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt

ENV MATRIX_SIZE 5000

CMD ["python", "main.py"]